﻿using System;
using BSA21_Lecture2.RabbitMQ.Wrapper;

namespace BSA21_Lecture2.Pinger
{
    class Ping
    {
        static IWorker worker = new Worker();
        static void Main()
        {
            Console.Title = "PINGER";
            worker.Do(true, "game", "ping_queue", "pong_queue", "ping", Extension.RandomInt(1, 16));
        }
    }
}
