﻿namespace BSA21_Lecture2.RabbitMQ.Wrapper
{
    public interface IWorker
    {
        void Do(bool starting, string exchange, string listenableRoutingKey, string sentRoutingKey, string message, int color);
    }
}
