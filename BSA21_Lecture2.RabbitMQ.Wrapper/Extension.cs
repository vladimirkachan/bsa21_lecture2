﻿using System;

namespace BSA21_Lecture2.RabbitMQ.Wrapper
{
    public static class Extension
    {
        static readonly Random random = new();
        public static int RandomInt(int min, int max)
        {
            return random.Next(min, max);
        }
        public static void Print(this string s, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.Write(s);
            Console.ResetColor();
        }
        public static void PrintLine(this string s, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(s);
            Console.ResetColor();
        }
    }
}
