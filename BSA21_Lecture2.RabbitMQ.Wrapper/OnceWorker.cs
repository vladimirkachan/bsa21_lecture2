﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BSA21_Lecture2.RabbitMQ.Wrapper
{
    public class OnceWorker : IWorker
    {
        public void Do(bool starting, string exchange, string listenableRoutingKey, string sentRoutingKey, string message, int color)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            Helper listener = new(channel, exchange, listenableRoutingKey);
            Helper sender = new(channel, exchange, sentRoutingKey, message, color);
            var queueName = channel.QueueDeclare(sentRoutingKey, false, false, false).QueueName;
            $"Queue name: {queueName}".PrintLine((ConsoleColor)color);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += listener.ListenQueue;
            consumer.Received += sender.SendMessageToQueue;
            if (starting) sender.SendMessageToQueue(null, null);
            channel.BasicConsume(queueName, true, consumer);
            Console.ReadKey();
        }
    }
}
