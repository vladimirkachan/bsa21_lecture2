﻿using System;

namespace BSA21_Lecture2.RabbitMQ.Wrapper
{
    public class Message
    {
        public string Text {get; set;}
        public ConsoleColor Color {get; set;}
    }
}
