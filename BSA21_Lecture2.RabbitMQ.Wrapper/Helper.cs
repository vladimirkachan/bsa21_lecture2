﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BSA21_Lecture2.RabbitMQ.Wrapper
{
    public class Helper : IDisposable
    {
        readonly IModel channel;
        public int Timeout {get; set;}
        public string Exchange {get; set;}
        public string RoutingKey {get; set;}
        public string MessageText {get; set;}
        public ConsoleColor ConsoleColor {get; set;}

        public Helper(IModel channel, string exchange, string routingKey, string messageText = "", int color = 7, int timeout = 2500)
        {
            this.channel = channel;
            Exchange = exchange;
            RoutingKey = routingKey;
            Timeout = timeout;
            MessageText = messageText;
            ConsoleColor = (ConsoleColor)color;
        }

        public void ListenQueue(object sender, BasicDeliverEventArgs e)
        {
            var bytes = Encoding.UTF8.GetString(e.Body.ToArray());
            Message message = JsonSerializer.Deserialize<Message>(bytes);
            var color = message.Color;
            $"{message.Text} ".Print(color);
            for (int t = 0, span = 100; t <= Timeout; t += span)
            {
                ". ".Print(color);
                Thread.Sleep(span);
            }
            "*".PrintLine(color);
        }
        public void SendMessageToQueue(object sender, BasicDeliverEventArgs e)
        {
            string text = $"{MessageText} {DateTime.Now}";
            var json = JsonSerializer.Serialize(new Message {Text = text, Color = ConsoleColor});
            var body = Encoding.UTF8.GetBytes(json);
            channel.BasicPublish(Exchange, RoutingKey, null, body);
        }
        public void Dispose()
        {
            channel.Dispose();
        }
    }
}
