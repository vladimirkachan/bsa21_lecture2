﻿using System;
using BSA21_Lecture2.RabbitMQ.Wrapper;

namespace BSA21_Lecture2.Ponger
{
    class Pong
    {
        static IWorker worker = new Worker();
        static void Main()
        {
            Console.Title = "PONGER";
            worker.Do(false, "game", "pong_queue", "ping_queue", "pong", Extension.RandomInt(1, 16));
        }
    }
}
